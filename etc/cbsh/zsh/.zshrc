source ~/znap/zsh-snap/znap.zsh
# XDG Variables
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache


# Initialise Znap

# Pull in some useful packages
znap source marlonrichert/zsh-autocomplete
znap source zsh-users/zsh-syntax-highlighting

#Fix No Such `Suffix Alias` thingie hopefully
zstyle ':autocomplete:*' min-input 1  # characters

# Make Home,End and Del keys work for kitty 
bindkey "^[[H"  beginning-of-line
bindkey "^[[4~"  end-of-line
bindkey "^[[P" delete-char


#Turn on auto cd because cd-ing is for posers
setopt auto_cd

# Initialise Starship Prompt
eval "$(starship init zsh)"

# Use powerline
USE_POWERLINE="true"

# Pull In Path so composer and such work as intended.
export PATH=$HOME/.local/opt/evolv/escribe-suite:/usr/local/bin:$HOME/.config/composer/vendor/bin/:$HOME/.config/doomemacs/bin/:$HOME/.cask/bin:$PATH

# Simple Aliases to edit and reload the ZSH Config File
alias zshconf="vim ~/.zshrc"
alias relzsh="source ~/.zshrc"
alias ls="exa -l"
# Set pywal colours on startup
# (cat ~/.cache/wal/sequences &)

# just for fun,show neofetch because im a cool kid or smth idk man
neofetch 
