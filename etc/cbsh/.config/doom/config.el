(add-hook 'org-mode-hook 'org-display-inline-images) ; Makes sure org displays inline images, if there are images in the document.

(setq user-full-name "Cameron Miller"
      user-mail-address "cameron@codecameron.dev")

(setq doom-font (font-spec :family "SauceCodePro Nerd Font" :size 15)
      doom-variable-pitch-font (font-spec :family "Fira Code Nerd Font" :size 15)
      doom-big-font (font-spec :family "SauceCodePro Nerd Font" :size 22))

(set-frame-parameter (selected-frame) 'alpha '(100))
(add-to-list 'default-frame-alist'(alpha . (100)))

(define-globalized-minor-mode global-rainbow-mode rainbow-mode
  (lambda () (rainbow-mode 1)))
(global-rainbow-mode 1 )

(add-hook 'org-mode-hook (lambda () (org-superstar-mode 1)))

(map! :leader
      :desc "Org Babel Tangle" "m B" #'org-babel-tangle)
(after! org
  (setq org-directory "~/Documents/org/"
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆")
        org-log-done 'time
        org-hide-emphasis-markers t
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("google" . "http://www.google.com/search?q=")
            ("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "BLOG(b)"           ; Blog writing assignments
             "PROJ(p)"           ; A project that contains other tasks
             "WAIT(w)"           ; Something is holding up this task
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" )))) ; Task has been cancelled

(use-package org-roam
  :custom
  (org-roam-directory "~/.config/doom/OrgRoam/Notes")
  :config
  (org-roam-setup))
(map! :leader
      :desc "Toggle Org Roam Buffer" "n l" #'org-roam-buffer-toggle)
(map! :leader
      :desc "Find a Node in Org Roam" "n f" #'org-roam-node-find)
(map! :leader
      :desc "Insert a Node into Org Roam" "n i" #'org-roam-node-insert)
(map! :leader
      :desc "Org Roam Completion at current Cursor Position" "n I" #'completion-at-point)

(after! org
  (setq org-agenda-files
        '("~/.config/doom/org/agenda.org")))

(setq
   ;; org-fancy-priorities-list '("[A]" "[B]" "[C]")
   ;; org-fancy-priorities-list '("❗" "[B]" "[C]")
   org-fancy-priorities-list '("🟥" "🟧" "🟨")
   org-priority-faces
   '((?A :foreground "#ff6c6b" :weight bold)
     (?B :foreground "#98be65" :weight bold)
     (?C :foreground "#c678dd" :weight bold))
   org-agenda-block-separator 8411)

(setq org-agenda-custom-commands
      '(("v" "A better agenda view"
         ((tags "PRIORITY=\"A\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "High-priority unfinished tasks:")))
          (tags "PRIORITY=\"B\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Medium-priority unfinished tasks:")))
          (tags "PRIORITY=\"C\""
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Low-priority unfinished tasks:")))
          (tags "birthdays"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Birthdays:")))
          (tags "customtag"
                ((org-agenda-skip-function '(org-agenda-skip-entry-if 'todo 'done))
                 (org-agenda-overriding-header "Tasks marked with customtag:")))

          (agenda "")
          (alltodo "")))))

(setq password-cache-expiry nil)
(setq ange-ftp-try-passive-mode t)

(defun connect-to-codecameron ()
(interactive)
(dired "/ssh:cabooshy@codecameron.dev:/"))

(defun connect-to-nextcloud ()
  (interactive)
  (dired "/ssh:root@192.168.1.80:/"))

(defun connect-to-cabooshyy ()
  (interactive)
  (dired "/ssh:ubuntu@cabooshyy.xyz:/"))

(defun connect-to-impact-express ()
  (interactive)
  (dired "/ftp:ftp_impactexpress_co_uk@upload.lcn.com:/web/wp-content/themes/impact-express-current/"))

(setq lsp-intelephense-completion-insert-use-declaration t)

(map! :leader
      (:prefix ("d" . "dired")
       :desc "Open dired" "d" #'dired
       :desc "Dired jump to current" "j" #'dired-jump)
      (:after dired
       (:map dired-mode-map
        :desc "Peep-dired image previews" "d p" #'peep-dired
        :desc "Dired view file" "d v" #'dired-view-file)))

(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-open-file ; use dired-find-file instead of dired-open.
  (kbd "m") 'dired-mark
  (kbd "t") 'dired-toggle-marks
  (kbd "u") 'dired-unmark
  (kbd "C") 'dired-do-copy
  (kbd "D") 'dired-do-delete
  (kbd "J") 'dired-goto-file
  (kbd "M") 'dired-do-chmod
  (kbd "O") 'dired-do-chown
  (kbd "P") 'dired-do-print
  (kbd "R") 'dired-do-rename
  (kbd "T") 'dired-do-touch
  (kbd "Y") 'dired-copy-filenamecopy-filename-as-kill ; copies filename to kill ring.
  (kbd "+") 'dired-create-directory
  (kbd "-") 'dired-up-directory
  (kbd "% l") 'dired-downcase
  (kbd "% u") 'dired-upcase
  (kbd "; d") 'epa-dired-do-decrypt)

(setq display-line-numbers-type 'relative)

(setq doom-theme 'catppuccin-frappe)

(map! :leader
      :desc "Load a New Theme from list of Installed themes." "h t" #'counsel-load-theme)

(custom-set-faces!
  '(doom-modeline-buffer-modified :foreground "purple"))

(setq initial-buffer-choice "~/.config/doom/start.org")

(define-minor-mode start-mode
  "Provide functions for custom start page."
  :lighter " start"
  :keymap (let ((map (make-sparse-keymap)))
          ;;(define-key map (kbd "M-z") 'eshell)
            (evil-define-key 'normal start-mode-map
              (kbd "1") '(lambda () (interactive) (find-file "~/.config/doom/config.org"))
              (kbd "2") '(lambda () (interactive) (find-file "~/.config/doom/init.el"))
              (kbd "3") '(lambda () (interactive) (find-file "~/.config/doom/packages.el"))
              (kbd "4") '(lambda () (interactive) (find-file "~/.config/doom/eshell/aliases"))
              (kbd "5") '(lambda () (interactive) (find-file "~/.config/doom/eshell/profile")))
          map))
(add-hook 'start-mode-hook 'read-only-mode) ;makes the Org file used for the start page RO, use SPC t r to toggle read-only and make it RW
(provide 'start-mode)

(setq eshell-aliases-file "~/.doom.d/aliases")
(setq shell-file-name "/usr/bin/bash")
(setq tramp-terminal-type "tramp")

(setq ivy-posframe-display-functions-alist
      '((swiper                     . ivy-posframe-display-at-frame-top-center)
        (complete-symbol            . ivy-posframe-display-at-frame-top-center)
        (counsel-M-x                . ivy-posframe-display-at-frame-top-center)
        (counsel-esh-history        . ivy-posframe-display-at-frame-top-center)
        (counsel-describe-function  . ivy-posframe-display-at-frame-top-center)
        (counsel-describe-variable  . ivy-posframe-display-at-frame-top-center)
        (counsel-find-file          . ivy-posframe-display-at-frame-top-center)
        (counsel-recentf            . ivy-posframe-display-at-frame-top-center)
        (counsel-register           . ivy-posframe-display-at-frame-top-center)
        (dmenu                      . ivy-posframe-display-at-frame-top-center)
        (t                        . ivy-posframe-display))
      ivy-posframe-height-alist
      '((swiper . 20)
        (t . 15)))
(ivy-posframe-mode 1) ; 1 enables posframe-mode, 0 disables it

(map! :leader
      (:prefix ("v" . "Ivy")
       :desc "Ivy push view" "v p" #'ivy-push-view
       :desc "Ivy switch view" "v s" #'ivy-switch-view))
