(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#282a36" "#ff5555" "#50fa7b" "#f1fa8c" "#61bfff" "#ff79c6" "#8be9fd" "#f8f8f2"])
 '(custom-safe-themes
   '("bdf2b5b617ea284bf2635f28509b3a5ce3fb1ed82cac00196a71cd8403fa3e60" "d1d43f2ab7b2f361997a8e3d699ccb8bde29a404fd864d175c9ea8d05ff8fb6d" "06567a6d869ad7667031bf5b46bda2eb4ca2c0fab70d41113fab2a58d317d133" "843ad7d4b26a579aea0c45c77ce4957fab2d3b3abdf347a17f66bf6b633f4d1e" "fe2220d2253aabad2446b873e493e328edf68a094a41ff8ef2a5756dd0b02a02" "c1ec1e6a8ac1a9600340ca6b6a5a4450dacbfdcb9872c5df4ad985e103f44483" "419dea6eb4f80a4ac631ae3032153875e64f3128fe85cdf6e62b50866017374b" "4ec2968e4c632edc7fe1c7528277cee60aa00aafad2eefcb9b4901c2bba596bb" "be832913de8c1e270de8b046abb428b6d26e4ae6c7f317a86e8503e550053e8d" "b027539ccd7783e7c1bae8da3dd2b76973854f7647b8b6ef469026808a4869fa" "96afbd1a57dad6800b2e02840b68f14ce1e32b43f7044a68eb1e51535d6a1f8e" "a7e347a3d4774ba493d2adc72f6358603eeb06601fe9624a9b25f3ce8a72b1e8" "cf78eca89646f162fbfcbcc87b0462cac9684ad0d13fb53b561cde03ed7c5f6d" "84ef619f009264a9efd52766386300a5687f87ff7b8bb529ee64110b54973faf" "acc6f8743a663c1bc085755152fcf6ed40e1c3dc8e8b093c4847375bbf48776a" "4b90c5a7d7e3859e910e0052e9623918c63e7b8e16716fa7366504ad3bc88985" "50bf5b5d66cbbba9f94d351cab6614287e2a1383b88aa185cb65406e2ddb46d4" "32302db142cf7e9b3978e096dd6684052970e2918c98087e37c3607e51322ada" "20100bd4308800ea82700e10be0c2092a948b548c90c23cd0c3dbfd5310628ad" "c4ac99cc06c3064b73425afc526804504a171f64f227212ed4df8ebfeb72ff43" "ef9ae6064b806390a026ad4454e450a79356d9754a02acdfb02bcd4b619ce64c" "163351fa084155c2aeb4e16612b73e6cd4e16006f21791e11bee86f9783a5d7e" "4893671b4960a3608fb2a2dafda0c13219df44fccf766eb2e4083f14b6b77fc9" "fba2d6bcdcfc88250f3aa5c830c1331f902874a25c576f1d0e0bf290b1eefda6" "660207d3e0e84062455e9934dfa57bfd48874c3a767aa18dd8911484b0a26340" default))
 '(exwm-floating-border-color "#242530")
 '(fci-rule-color "#6272a4")
 '(highlight-tail-colors
   ((("#2c3e3c" "#2a3b2e" "green")
     . 0)
    (("#313d49" "#2f3a3b" "brightcyan")
     . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#1E2029" "#bd93f9"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1E2029" "#50fa7b"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1E2029" "#565761"))
 '(objed-cursor-color "#ff5555")
 '(pdf-view-midnight-colors (cons "#f8f8f2" "#282a36"))
 '(rustic-ansi-faces
   ["#282a36" "#ff5555" "#50fa7b" "#f1fa8c" "#61bfff" "#ff79c6" "#8be9fd" "#f8f8f2"])
 '(vc-annotate-background "#282a36")
 '(vc-annotate-color-map
   (list
    (cons 20 "#50fa7b")
    (cons 40 "#85fa80")
    (cons 60 "#bbf986")
    (cons 80 "#f1fa8c")
    (cons 100 "#f5e381")
    (cons 120 "#face76")
    (cons 140 "#ffb86c")
    (cons 160 "#ffa38a")
    (cons 180 "#ff8ea8")
    (cons 200 "#ff79c6")
    (cons 220 "#ff6da0")
    (cons 240 "#ff617a")
    (cons 260 "#ff5555")
    (cons 280 "#d45558")
    (cons 300 "#aa565a")
    (cons 320 "#80565d")
    (cons 340 "#6272a4")
    (cons 360 "#6272a4")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(doom-modeline-buffer-modified ((t (:foreground "purple")))))
(put 'customize-variable 'disabled nil)
