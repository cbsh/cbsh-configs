from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#282a36', '#282a36']
    grey            = ['#565761', '#565761']
    white           = ['#f8f8f2', '#f8f8f2']
    red             = ['#ff5555', '#ff5555']
    brightred       = ['#ffb86c', '#ffb86c']
    green           = ['#50fa7b', '#50fa7b']
    brightgreen     = ['#0189cc', '#0189cc']
    blue            = ['#61bfff', '#61bfff']
    darkblue        = ['#0189cc', '#0189cc']
    magenta         = ['#ff79c6', '#ff79c6']
    brightmagenta   = ['#bd93f9', '#bd93f9']
    cyan            = ['#8be9fd', '#8be9fd']
    brightcyan      = ['#8be9fd', '#8be9fd']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/cbsh-doom-dracula/cbsh-doom-dracula.png"
