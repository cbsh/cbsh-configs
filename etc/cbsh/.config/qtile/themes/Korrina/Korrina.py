#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#280e08', '#280e08']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#93112d', '#93112d']
    brightred       = ['#e71a46', '#e71a46']
    green           = ['#0f6d52', '#0f6d52']
    brightgreen     = ['#17ad84', '#17ad84']
    blue            = ['#4cb1ff', '#4cb1ff']
    darkblue        = ['#29618b', '#29618b']
    magenta         = ['#c33d10', '#c33d10']
    brightmagenta   = ['#ee6e23', '#ee6e23']
    cyan            = ['#e8d207', '#e8d207']
    brightcyan      = ['#f5ed99', '#f5ed99']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Korrina/BG.png"
