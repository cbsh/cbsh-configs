from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#00212C', '#00212C']
    grey            = ['#56697A', '#56697A']
    white           = ['#EEEEEE', '#EEEEEE']
    red             = ['#dc322f', '#dc322f']
    brightred       = ['#cb4b16', '#cb4b16']
    green           = ['#859900', '#859900']
    brightgreen     = ['#33aa99', '#33aa99']
    blue            = ['#405A61', '#405A61']
    darkblue        = ['#51afef', '#51afef']
    magenta         = ['#d33682', '#d33682']
    brightmagenta   = ['#6c71c4', '#6c71c4']
    cyan            = ['#204052', '#204052']
    brightcyan      = ['#46D9FF', '#46D9FF']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/cbsh-doom-solarized-dark/cbsh-doom-solarized-dark.png"
