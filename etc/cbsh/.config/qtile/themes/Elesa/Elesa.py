#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#3f3049', '#3f3049']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#953540', '#953540']
    brightred       = ['#ee5466', '#ee5466']
    green           = ['#285274', '#285274']
    brightgreen     = ['#4790cd', '#4790cd']
    blue            = ['#988e88', '#988e88']
    darkblue        = ['#515558', '#515558']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#ddd01e', '#ddd01e']
    brightcyan      = ['#f0ee79', '#f0ee79']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Elesa/BG.png"
