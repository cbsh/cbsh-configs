from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#241B30', '#241B30']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#87495a', '#87495a']
    brightred       = ['#B46278', '#B46278']
    green           = ['#542269', '#542269']
    brightgreen     = ['#9646B8', '#9646B8']
    blue            = ['#65538C', '#65538C']
    darkblue        = ['#4B3E69', '#4B3E69']
    magenta         = ['#723C87', '#723C87']
    brightmagenta   = ['#9C5CAC', '#9C5CAC']
    cyan            = ['#B04C9F', '#B04C9F']
    brightcyan      = ['#e1acff', '#e1acff']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/cbsh-doom-vibrant/cbsh-doom-vibrant.png"
