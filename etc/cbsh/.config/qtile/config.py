# [[file:README.org::*Imports][Imports:1]]
#   ____  ____  ____   _   _
#  / ___|| __ )/ ___| | | | |
# | |    |  _ \\___ \ | |_| |
# | |___ | |_) |___) ||  _  |
#  \____||____/|____/ |_| |_|
#
# NAME: CBSH -- QTile Config
# DESC: My Personal QTile Configuration File for use with CBSH
#

import os, sys
import subprocess
from libqtile import bar, layout, qtile, hook

from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration, PowerLineDecoration

from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
from typing import List  # noqa: F401

from currentTheme import Colors, Wallpaper, Fonts

home = os.path.expanduser('~')
# Imports:1 ends here

# [[file:README.org::*Autostart][Autostart:1]]
@hook.subscribe.startup_once
def start_once():
        subprocess.call([home + '/.config/qtile/autostart.sh'])
# Autostart:1 ends here

# [[file:README.org::*QTile Colours][QTile Colours:1]]
# Qtile Colours
# Default Included Colour Schemes are:
# CBSHVibrant -- The Default colour scheme
# CBSHBlackHole -- Alternate colour scheme based off of the Black Hole Wallpaper from Ubuntu 16.04 Xenial Xerus
# DoomOne -- the Default DOOM Emacs colours
# Dracula -- Colours from the Dracula Colour Scheme
# Solarized Dark -- Colours from Solarized Dark
#
# Set the desired colour theme from above with dm-theme dmenu script

colors = [
    Colors.black,         #0
    Colors.grey,          #1
    Colors.white,         #2
    Colors.red,           #3
    Colors.brightred,     #4
    Colors.green,         #5
    Colors.brightgreen,   #6
    Colors.blue,          #7
    Colors.darkblue,      #8
    Colors.magenta,       #9
    Colors.brightmagenta, #10
    Colors.cyan,          #11
    Colors.brightcyan,    #12
]
# QTile Colours:1 ends here

# [[file:README.org::*Variables][Variables:1]]
mod = "mod4"  # Sets Mod Key to Super/Windows
#if qtile.core.name == "x11":
terminal = "st"  # My Terminal of Choice for X
#elif qtile.core.name == "wayland":
#    terminal = "sakura" # Terminal for Wayland
# Variables:1 ends here

# [[file:README.org::*Qtile Extras][Qtile Extras:1]]
border = {
    "decorations": [
        BorderDecoration( border_width = [2,0,0,0], colour = colors[4], padding_x = 5, padding_y = None )
    ]
}
powerline = {
    "decorations": [
        PowerLineDecoration(path="arrow_right")
    ]
}
# Qtile Extras:1 ends here

# [[file:README.org::*My Keybindings][My Keybindings:1]]
keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
	desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), lazy.layout.decrease_nmaster(),
	desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), lazy.layout.increase_nmaster(),
	desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), lazy.layout.section_down(),
	desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), lazy.layout.section_up(),
	desc="Move window up"),

    # Treetab Controls
    Key([mod, "shift"], "h", lazy.layout.move_left(),
	desc="Move up a section (treetab)"),
    Key([mod, "shift"], "l", lazy.layout.move_right(),
	desc="Move down a section (treetab)"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
	desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
	desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
	desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(),
	desc="Grow window up"),

    Key([mod], "i", lazy.layout.grow(), desc="Grow Focused Window"),
    Key([mod], "m", lazy.layout.shrink(), desc="Shrink Focused Window"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod, "shift"], "space", lazy.layout.flip(), desc="Flip Layout"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "mod1"], "Return", lazy.layout.toggle_split(),
	desc="Toggle between split and unsplit sides of stack"),
    Key([mod, "shift"], "Return", lazy.spawn("dmenu_run -c -g 4 -l 15 -h 27"), desc="Run applications with dmenu"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below, as well as toggling floating
    Key([mod, "shift"], "t", lazy.window.toggle_floating(), desc="Toggle Floating on focused window"),
    Key([mod, "shift"], "m", lazy.window.toggle_minimize(), desc="Minimise windows"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "c", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.reload_config(), desc="Restart Qtile"),

    #There are two options here, either the GUI session manager application, or the dmenu-script
    Key([mod, "control"], "q", lazy.spawn("cbsh-session-manager"),
	desc="Shutdown Qtile"),

    # Emacs
    Key([mod], "e", lazy.spawn("emacsclient -c -s cbshmacs -a 'emacs'"),
	desc="Launch CBSHMacs"),
# My Keybindings:1 ends here

# [[file:README.org::*Keychords][Keychords:1]]
#Key([mod, "control"], "q", lazy.spawn(home + "/dmenu-scripts/scripts/dm-logout"),
      #    desc="Shutdown Qtile"),

      # Emacs KeyChords
#      KeyChord(["mod1"], "e", [
#	  Key([], "e", lazy.spawn("emacsclient -c -s cbshmacs -a 'emacs'"),
#	      desc="Launch CBSHMacs"),
#	  Key([], "v", lazy.spawn("emacsclient -c -s vanilla -a 'emacs'"),
#	      desc="Launch GNU Emacs"),
#	  Key([], "d", lazy.spawn("emacsclient -c -s doomemacs -a 'emacs'"),
#	      desc="Launch DOOM Emacs")]),

#      # Run launcher KeyChords
#      KeyChord([mod], "d",[
#	  Key([], "r", lazy.spawn(home + "/dmenu-scripts/scripts/dm-golaunch"), desc="Use golaunch to open .desktop applications via dmenu"),
#	  Key([], "e", lazy.spawn('emacsclient -cF "((visibility . nil))" -s cbshmacs -e "(emacs-app-launcher)"'), desc="Use counsel-linux-app to launch .desktop applications"),
#      ]),

      # Dmenu Script KeyChords
#      KeyChord([mod], "p",[
#	  Key([], "l", lazy.spawn(home + "/dmenu-scripts/scripts/dm-logout"), desc="Simple dmenu based logout menu"),
#	  Key([], "t", lazy.spawn(home + "/dmenu-scripts/scripts/dm-theme"), desc="Change the CBSH Theme using dmenu"),
#	  Key([], "c", lazy.spawn(home + "/dmenu-scripts/scripts/dm-config"), desc="open config files with dmenu"),
#	  Key([], "s", lazy.spawn(home + "/dmenu-scripts/scripts/dm-search"), desc="Search the Web with dmenu"),
#	  Key([], "u", lazy.spawn(home + "/dmenu-scripts/scripts/dm-usb"), desc="Mount and Unmount USB devices with dmenu")
#      ]),
# Keychords:1 ends here

# [[file:README.org::*Keychords][Keychords:2]]
Key([], "Print", lazy.spawn("flameshot screen --path Pictures/screenshots"), desc="Use Flameshot to capture the screen"),
    Key(["control"], "Print", lazy.spawn("flameshot full --path Pictures/screenshots"), desc="Use Flameshot to capture all screens"),
    Key(["shift"], "Print", lazy.spawn("flameshot gui --path Pictures/screenshots"), desc="Use the Flameshot GUI to capture the screen"),

    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle"), desc="Mute Audio"),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl --player=%any,brave play-pause"), desc="Play/Pause Audio"),
    Key([], "XF86AudioNext", lazy.spawn("playerctl --player=%any,brave next"), desc="Next Song"),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl --player=%any,brave previous"), desc="Previous Song"),
    Key([], "XF86AudioStop", lazy.spawn("playerctl --player=%any,brave stop"), desc="Stop Audio"),
]
# END KEYS
# Keychords:2 ends here

# [[file:README.org::*Groups (Workspaces)][Groups (Workspaces):1]]
groups = [
	Group("1", label="[WWW]", layout="monadtall"),
	Group("2", label="[DEV]", layout="treetab"),
	Group("3", label="[SYS]", layout="monadtall"),
	Group("4", label="[VBOX]", layout="monadtall"),
	Group("5", label="[GAME]", layout="monadtall"),
	Group("6", label="[CHAT]", layout="monadtall"),
	Group("7", label="[MUS]", layout="monadtall"),
]
for i in groups:
    keys.extend([
	# mod1 + letter of group = switch to group
	Key([mod], i.name, lazy.group[i.name].toscreen(),
	    desc="Switch to group {}".format(i.name)),

	Key([mod], 'F11', lazy.group['scratchpad'].dropdown_toggle('term')),
	Key([mod], 'F12', lazy.group['scratchpad'].dropdown_toggle('QShell')),


	# Move focused window to group.
	Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
	    desc="Switch to & move focused window to group {}".format(i.name)),
    ])


groups.append(
	ScratchPad("scratchpad",[
		DropDown("term", terminal, opacity=0.95),
		DropDown("QShell", "st -e qtile shell")]),
),
# Groups (Workspaces):1 ends here

# [[file:README.org::*Enabled Layouts][Enabled Layouts:1]]
layouts = [
    layout.MonadTall(margin=5),
    layout.Columns(margin=3),
    layout.Floating(),
    layout.Max(),
    layout.TreeTab(
    font = "SauceCodePro Nerd Font Bold",
    fontsize = 11,
    border_width = 0,
    bg_color = colors[0],
    active_bg = colors[12],
    active_fg = colors[0],
    inactive_bg = colors[6],
    inactive_fg = colors[2],
    padding_x = 8,
    padding_y = 6,
    sections = ["EMACS", "TERMINALS"],
    section_fg = colors[6],
    section_top = 15,
    section_bottom = 15,
    level_shift = 8,
    vspace = 3,
    panel_width = 280)]
# Enabled Layouts:1 ends here

# [[file:README.org::*My Widget Settings][My Widget Settings:1]]
widget_defaults = dict(
    font=Fonts.base, # Font choices are base and bold.
    fontsize=12,
    padding=2,
    background=colors[0],
    foreground=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
	     widget.TextBox(
		    text = "",
		    font = "Font Awesome 5 Brands",
		    padding = 5,
		    fontsize = 23,
		    background = colors[0],
		    mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(terminal)},
		    ),
	    widget.GroupBox(
		    font = Fonts.base,
		    use_mouse_wheel = False,
		    disable_drag = True,
		    margin_y = 3,
		    margin_x = 0,
		    padding_y = 5,
		    padding_x = 1,
		    borderwidth = 3,
		    foreground = colors[2],
		    background = colors[0],
		    active = colors[2],
		    inactive = colors[1],
		    highlight_color = colors[0],
		    highlight_method = "line",
		    this_current_screen_border = colors[4],
		    this_screen_border = colors[3],
		    other_current_screen_border = colors[6],
		    other_screen_border = colors[5],
		    **border

	    ),
	    widget.Sep(
		    linewidth = 0,
		    padding = 15,
		    foreground = colors[2],
		    background = colors[0],
		    ),

	    widget.WindowName(
		    foreground = colors[12],
		    background = colors[0],
		    padding = 0,
		    max_chars = 60,
		    **powerline
	    ),
	    widget.Clock(
		format='%b %d, %y | %H:%M',
		padding = 15,
		foreground=colors[2],
		background = colors[6],
		**powerline
	    ),
	    widget.TextBox(
		    text="",
		    font="Font Awesome 5 Free",
		    padding=0,
		    fontsize=15,
		    foreground = colors[0],
		    background = colors[12],
	    ),
	    widget.CheckUpdates(
		    update_interval = 600,
		    padding = 10,
		    distro = "Arch",
		    display_format = "Updates: {updates}",
		    colour_have_updates = colors[0],
		    colour_no_updates = colors[0],
		    no_update_string = "Up To Date!",
		    foreground = colors[0],
		    mouse_callbacks = {"Button1": lambda: qtile.cmd_spawn(terminal + " -e sudo pacman -Syu")},
		    background = colors[12],
		    **powerline
	    ),
	    widget.OpenWeather(
		padding = 10,
		app_key="PUT YOUR APP KEY HERE",
		cityid="PUT CITY ID HERE",
		format="{weather_details} {icon}: {main_temp} °{units_temperature}",
		foreground=colors[2],
		background = colors[6],
		**powerline
	    ),
	    widget.CurrentLayout(
		foreground = colors[0],
		background = colors[12],
		padding = 10,
		**powerline
	    ),
	    widget.Systray(
		padding = 5,
		background = colors[0]
	    ),
    ]
    return widgets_list
# My Widget Settings:1 ends here

# [[file:README.org::*Screen Setup and Floating Layouts][Screen Setup and Floating Layouts:1]]
def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    del widgets_screen1[9]
    return widgets_screen1

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2

screens = [
    Screen(
	wallpaper = Wallpaper.wallpaper, # Wallpaper is set inside the activated theme (by default its CBSHVibrant, you can change this to whatever you want.
	wallpaper_mode='fill',
	top=bar.Bar(widgets=init_widgets_screen2(), size=27, background=colors[0]),
    ),
    Screen(
	wallpaper = Wallpaper.wallpaper, # Wallpaper is set inside the activated theme (by default its CBSHVibrant, you can change this to whatever you want.
	wallpaper_mode='fill',
	top=bar.Bar(widgets=init_widgets_screen1(), size=27, background=colors[0]),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
	 start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
	 start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title="emacs-run-launcher"), # make emacs-run-launcher (counsel-linux-app) float
    Match(title="Conky"), # make Conky windows float
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
# Screen Setup and Floating Layouts:1 ends here
