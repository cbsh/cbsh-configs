#!/usr/bin/env bash

# Make Multi Monitor Work, will need to change for your own desktop, comment out if using a single monitor
xrandr --output HDMI1 --primary --auto --output DP1 --right-of HDMI1 --auto

# i've found that sometimes the script will race-condition itself into not running some things, so by sleeping the script
# after starting the apps in the background it helps stop that.
sleep 0.5
lxsession &
volumeicon &

sleep 0.5
picom -experimental-backends --backend glx --xrender-sync-fence &

sleep 0.5
/usr/bin/dunst &
pcmanfm -d &

sleep 0.5
/usr/bin/emacs --daemon & #CBSHMacs Config 
/usr/bin/emacs --with-profile doomemacs --daemon & # DOOM Emacs
playerctld daemon &
nm-applet &
