= IMPORTANT: Remember by default, the Modkey is set to the Super (Windows) Key, this is changable in the config. =

== Switching Between Windows ==
| KEYBIND    | Associated Action       |
|------------|-------------------------|
| MODKEY + H | Move Focus To the Left  |
| MODKEY + J | Move Focus Down         |
| MODKEY + K | Move Focus Up           |
| MODKEY + L | Move Focus to the Right |

== Move Windows Between Columns / Move Up/Down in the Current Stack ==
| KEYBIND            | Associated Action        |
|--------------------|--------------------------|
| MODKEY + Shift + H | Move Window to the Left  |
| MODKEY + Shift + J | Move Window Down         |
| MODKEY + Shift + K | Move Window Up           |
| MODKEY + Shift + L | Move Window to the Right |

== Grow/Shrink Windows (Resizing windows),==
=== If the current window is on the edge of the screen and the direction would be the screen edge, the window will shrink ===
| KEYBIND              | Associated Action              |
|----------------------|--------------------------------|
| MODKEY + Control + H | Grow Window to the left        |
| MODKEY + Control + J | Grow Window Down               |
| MODKEY + Control + K | Grow Window Up                 |
| MODKEY + Control + L | Grow Window to the Right       |
| MODKEY + N           | Normalise (Reset) Window Sizes |

== Toggle between split and unsplit sides of the stack ==
| KEYBIND                | Associated Action             |
|------------------------|-------------------------------|
| MODKEY + Shift + ENTER | Toggle Between Sides of Stack |

== Toggle between Tiling Layouts, and whether a window floats ==
| KEYBIND            | Associated Action                 |
|--------------------|-----------------------------------|
| MODKEY + Shift + T | Toggle Floating on Focused Window |
| MODKEY + TAB       | Toggle Between Enabled Layouts    |

== Emacs Keychords ==
| KEYBIND       | Associated Action         |
|---------------|---------------------------|
| Control + E E | Launch DOOM Emacs         |
| Control + E B | Open iBuffer inside Emacs |
| Control + E D | Open Dired inside Emacs   |
| Control + E V | Open Vterm inside Emacs   |

== Run Launcher (Dmenu, Rofi) Keychords ==
| KEYBIND      | Associated Action                                                  |
|--------------|--------------------------------------------------------------------|
| MODKEY + D D | Open Dmenu                                                         |
| MODKEY + D R | Open Rofi (Not Installed by default, used as a backup/alternative) |

== Dmenu-Script Keychords ==
| KEYBIND      | Associated Action                                                                        |
|--------------|------------------------------------------------------------------------------------------|
| MODKEY + P C | Runs dm-config script (allows you to open a selection of config files in emacs)          |
| MODKEY + P T | Runs dm-theme script (Changes the Qtile theme, will eventially change terminal and such) |
| MODKEY + P L | Runs dm-logout script (same as pressing Ctrl+Mod+Q to logout of Qtile)                   |

== Vimwiki Keybinds (Help Pages for CBSH) ==
| KEYBIND       | Associated Action                     |
|---------------|---------------------------------------|
| Control + H H | Opens Main Vimwiki File               |
| Control + H Q | Opens Qtile Keybinds File (This File) |
| Control + H E | Open Doom Emacs Keybinds File         |
| Control + H V | Open Vim Mappings File                |
| Control + H T | Open Themes Information File          |

== General Keybinds ==
Media keys use playerctl to function, the install script should have downloaded this already, if not, run sudo pacman -S playerctl
| KEYBIND              | Associated Action                              |
|----------------------|------------------------------------------------|
| MODKEY + Alt + L     | Lock Screen with Betterlockscreen              |
| Shift + Print        | Take Screenshot with Flameshot                 |
| XF86AudioMute        | Mute Audio via amixer                          |
| XF86AudioPlay        | Play/Pause Song                                |
| XF86AudioNext        | Next Song                                      |
| XF86AudioPrev        | Previous Song                                  |
| XF86AudioStop        | Stop Audio                                     |
| MODKEY + Enter       | Open Terminal (set to ST by default)           |
| MODKEY + 1-9         | Switch to group 1-9                            |
| MODKEY + SHIFT + 1-9 | Switch to and move focused window to group 1-9 |
| MODKEY + F11         | Open Terminal Scratchpad                       |
| MODKEY + F12         | Open Qtile Shell (QShell) Scratchpad           |
| MODKEY + Shift + C   | Kill (Close) Focused Window                    |
| MODKEY + Control + R | Restart Qtile                                  |
| MODKEY + Control + Q | Logout of Qtile                                |
